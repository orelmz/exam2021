// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{ 
    apiKey: "AIzaSyBnSi27vqH1kO_suSD4lCcovTUmJ5c_Qm4",
    authDomain: "test-8a999.firebaseapp.com",
    projectId: "test-8a999",
    storageBucket: "test-8a999.appspot.com",
    messagingSenderId: "130061972075",
    appId: "1:130061972075:web:73fb7f9751ca677c04772e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
