export interface Student {
    mail:string;
    Grade_math: number;
    Psychometric : number;
    Tuition_paid: string;
    id?:string;
    saved?:Boolean;
    result:string;     
}


