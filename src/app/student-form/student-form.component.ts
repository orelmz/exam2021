import { Router } from '@angular/router';
import { PredictionService } from './../prediction.service';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { Student } from '../interfaces/student';
import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  //@Input() book: Book;
  Grade_math: number;
 Psychometric: number;
  id: string;
 Tuition_paid: string;
  formType: string;
  

  Error1:boolean = false;
  Error2:boolean = false;
  userId;
  mail;
  res;
  prediction = false;
  buttonText:String = 'Add student'; 
  submit(){
    this.studentService.addStudent(this.mail,this.Grade_math, this.Psychometric,this.Tuition_paid, this.res)
    this.router.navigate(['/students']);

  }
  onSubmit(){ 

  }  


  // awsSender(){
 
  //   this.res = this.predictionService.predict(this.Grade_math, this.Psychometric, this.Tuition_paid);
  //   console.log(this.res)
  //   this.predict=true;
  // }
  
predict(Grade_math, Psychometric, Tuition_paid){
  this.predictionService.predict(Grade_math, Psychometric,Tuition_paid).subscribe(
      res =>{
        console.log(res);
        if (res>0.5){
          var result = "Will not fall out"}
          else{
            var result = "May fall out"
          }
          this.res = result;
    })
   
  }
  constructor(private studentService:StudentsService,private auth:AuthService,private predictionService:PredictionService,private router:Router) { }


  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.mail = user.email;

       }
    )
    if(this.formType == 'Add student'){
      this.buttonText = 'Add';
    }
  }

}
