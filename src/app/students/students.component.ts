import { Student } from './../interfaces/student';
import { StudentsService } from '../students.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictionService } from '../prediction.service';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;
  mail;
  students:Student[];
  students$;
  addStudentFormOpen = false;
  rowToEdit:[]; 
  panelOpenState = false;

  // studentToEdit:Student = {Grade_math:null, Psychometric:null, Tuition_paid:null};


  
  // moveToEditState(index){
  //   console.log(this.students[index].Grade_math);
  //   this.studentToEdit.Grade_math = this.students[index].Grade_math;
  //   this.studentToEdit.Psychometric = this.students[index].Psychometric;
  //   this.studentToEdit.Tuition_paid = this.students[index].Tuition_paid;
  //   this.rowToEdit = index; 
  // }

  // updateStudent(){
  //   let id = this.students[this.rowToEdit].id;
  //   this.studentsService.updateStudent(this.userId,id, this.studentToEdit.Grade_math,this.studentToEdit.Psychometric,this.studentToEdit.Tuition_paid);
  //   this.rowToEdit = null;
  // }

  deleteStudent(id){
    this.studentsService.deleteStudent(id);
  }



  constructor(private studentsService:StudentsService,
    public auth:AuthService
    ) { }

    ngOnInit(): void {
    
   
      this.students$ = this.studentsService.getStudent();
      this.students$.subscribe(
        docs =>{
          this.students = [];
          for(let document of docs){
            const student:Student = document.payload.doc.data();
            
            student.id = document.payload.doc.id;
            this.students.push(student);
          }
        }
      )
    }
}
