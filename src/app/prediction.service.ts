import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://hta4cjwiei.execute-api.us-east-1.amazonaws.com/beta/'; 

  predict(Grade_math:number, Psychometric:number, Tuition_paid:string){
    let json = {
      "data":{
        "Grade_math":Grade_math,
        "Psychometric":Psychometric,
        "Tuition_paid":Tuition_paid
      }
    }
    
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res=>{
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','')
        final = final.replace(']','')
        return final
      })
    );
    
  }

  constructor(private http:HttpClient) { }
}