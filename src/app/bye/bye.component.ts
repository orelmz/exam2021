import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-bye',
  templateUrl: './bye.component.html',
  styleUrls: ['./bye.component.css']
})
export class ByeComponent implements OnInit {
  email:any = "";
  afAuth: any;


  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.user.subscribe(
          user => {
            this.email =user?.email;
      }
    )

  }

}

